'use strict';

const gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    // sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require("browser-sync"),
    reload = browserSync.reload;

const path = {
    build: {
        html: 'build/',
        js: 'build/assets/js/',
        css: 'build/assets/css/',
        img: 'build/assets/img/',
        font: 'build/assets/fonts/'
    },
    src: {
        html: 'source/*.html',
        js: 'source/js/**/*.js',
        css: 'source/style/style.scss',
        img: 'source/img/**/*.*',
        font: 'source/fonts/**/*.*'
    },
    watch: {
        html: 'source/**/*.html',
        js: 'source/js/**/*.js',
        css: 'source/style/**/*.scss',
        img: 'source/img/**/*.*',
        font: 'source/fonts/**/*.*'
    },
    clean: './build'
};

const config = {
    server: {
        baseDir: './build'
    },
    tunnel: true,
    host: 'localhost',
    port: '8080',
    logPrefix: 'frontend'
};

gulp.task('html:build', function() {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream: true}));
});

gulp.task('js:build', function() {
    gulp.src(path.src.js)
        .pipe(rigger())
        // .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({stream: true}));
});

gulp.task('style:build', function() {
    gulp.src(path.src.css)
        .pipe(sass().on('error', sass.logError))
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));

    gulp.src('./bower_components/bootstrap/scss/bootstrap.scss')
        .pipe(sass({errLogToConsole: true}))
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream: true}));
});

gulp.task('image:build', function() {
    gulp.src(path.src.img)
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            // use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({stream: true}))
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.font)
        .pipe(gulp.dest(path.build.font))
});

gulp.task('build', [
    'html:build',
    'js:build',
    'style:build',
    'image:build',
    'fonts:build'
]);

gulp.task('watch', function() {
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.css], function(event, cb) {
        gulp.start('style:build')
    });
    watch('./bower_components/bootstrap/scss/bootstrap.scss', function(event, cb) {
        gulp.start('style:build')
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
    });
    watch([path.watch.font], function(event, cb) {
        gulp.start('fonts:build');
    });
});

gulp.task('webserver', function() {
    browserSync(config);
});

gulp.task('clean', function(cb) {
    rimraf(path.clean, cb);
    reload({stream: true});
});

gulp.task('default', ['build', 'webserver', 'watch']);
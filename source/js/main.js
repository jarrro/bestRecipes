'use strict';

(function() {
    progress.classList.add('show');
    
    document.addEventListener('DOMContentLoaded', function() {
        progress.classList.remove('show');

        var textBlock = document.getElementsByClassName('text-block');

        setTimeout(function() {
            [].forEach.call(textBlock, function(el) {
                el.style.opacity = 1;
            });
        },500);
    });

    
})();

function fromValue(val) {
    var toInput = document.getElementsByName('to');
    
    toInput[0].min = val;

    from.innerHTML = val;
}

function toValue(val) {
    var fromInput = document.getElementsByName('from');

    fromInput[0].max = val;

    to.innerHTML = val;
}

function search(val) {
    var select = document.getElementsByName('select');

    console.log(select);

    for (var i = 0; i < select.options.length; i++) {
        if(select.options[i].innerHTML == val) {
            select.options[i].selected = true;

            console.log(select.options[i]);
        }
    }
}

function sendData() {
    var body = {
        from: from.innerHTML,
        to: to.innerHTML,
        date: date.value
    };

    console.log([body]);

    var xhr = new XMLHttpRequest();
    
    xhr.open('POST', 'http://someurl.com/post', true);

    xhr.send(body);

}